#if ARDUINO < 100 
#include <WProgram.h> 
#else 
#include <Arduino.h> 
#endif

#ifndef BUTTON_H_
#define BUTTON_H_

class Button
{
public:
  Button(int delay, int mode, int state) {
    setDebounceDelay(delay);
    setPinInputMode(mode);
    setPinDownState(state);
  }
  Button(int mode, int state) {
    setDebounceDelay(50);
    setPinInputMode(mode);
    setPinDownState(state);
  }
  Button() {
    setDebounceDelay(50);
    setPinInputMode(INPUT);
    setPinDownState(HIGH);
  }
  int getState();
  long getStateElapsedTime();
  long getDebounceElapsedTime();
  bool isPressed() ;
  void setPin(int digitalPin);
  void setPinInputMode(int mode);
  void setPinDownState(int state);
  void setDebounceDelay(int delay);
private:
  long lastDebounceTime;
  long lastPressedTime;
  long debounceDelay;
  int lastButtonState;
  int buttonState;
  int buttonDownState;
  int pinId;
  int pinInputMode;
  void poll();
};



#endif /* BUTTON_H_ */
