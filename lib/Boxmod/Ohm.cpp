/*
 * ohm.cpp
 *
 *  Created on: Jul 24, 2015
 *      Author: Igneous
 */

#include <math.h>
#include "Ohm.h"

// Public Class Methods

void Ohm::setWatts(float watts) {
  if (setitems < 2 && wattsSet == false) {
      setitems += 1;
      this->watts = watts;
      wattsSet = true;
  };
  if (wattsSet == true) {
      this->watts = watts;
  };
}

void Ohm::setVolts(float volts) {
  if (setitems < 2 && voltsSet == false) {
      setitems += 1;
      this->volts = volts;
      voltsSet = true;
  };
  if (voltsSet == true) {
      this->volts = volts;
  };
}

void Ohm::setAmps(float amps) {
  if (setitems < 2 && ampsSet == false) {
      setitems += 1;
      this->amps = amps;
      ampsSet = true;
  };
  if (ampsSet == true) {
      this->amps = amps;
  };
}

void Ohm::setOhms(float ohms) {
  if (setitems < 2 && ohmsSet == false) {
      setitems += 1;
      this->ohms = ohms;
      ohmsSet = true;
  }
  if (ohmsSet == true) {
      this->ohms = ohms;
  };
}

float Ohm::getWatts() {
  float retval = 0.00;

  if (wattsSet) {
    retval = watts;
  }
  if (voltsSet && ampsSet) {
    retval = Ohm::va2w(volts, amps);
  }
  if (ampsSet && ohmsSet) {
    retval = Ohm::ra2w(ohms, amps);
  }
  if (voltsSet && ohmsSet) {
    retval = Ohm::vr2w(volts, ohms);
  }

  return retval;
}

float Ohm::getAmps() {
  float retval = 0.00;

  if (ampsSet) {
    retval = amps;
  }
  if (voltsSet && ohmsSet) {
    retval = Ohm::vr2a(volts, ohms);
  }
  if (wattsSet && voltsSet) {
    retval = Ohm::wv2a(watts, volts);
  }
  if (wattsSet && ohmsSet) {
    retval = Ohm::wr2a(watts, ohms);
  }

  return retval;
}

float Ohm::getVolts() {
  float retval = 0.00;

  if (voltsSet) {
    retval = volts;
  }
  if (ampsSet && ohmsSet) {
    retval = Ohm::ra2v(ohms, amps);
  }
  if (wattsSet && ohmsSet) {
    retval = Ohm::rw2v(ohms, watts);
  }
  if (wattsSet && ampsSet) {
    retval = Ohm::wa2v(watts, amps);
  }

  return retval;
}

float Ohm::getOhms() {
  float retval = 0.00;

  if (ohmsSet) {
    retval = ohms;
  }
  if (voltsSet && ampsSet) {
    retval = Ohm::va2r(volts, amps);
  }
  if (voltsSet && wattsSet) {
    retval = Ohm::vw2r(volts, watts);
  }
  if (wattsSet && ampsSet) {
    retval = Ohm::wa2r(watts, amps);
  }

  return retval;
}

// Private Helpers

float Ohm::va2w(float volts, float amps) {
  float wattage = volts * amps;
  return wattage;
}

float Ohm::ra2w(float resistance, float amps) {
  float wattage = pow(amps,2) * resistance;
  return wattage;
}

float Ohm::vr2w(float volts, float resistance) {
  float wattage = pow(volts,2) / resistance;
  return wattage;
}

float Ohm::vr2a(float volts, float resistance) {
  float amperage = volts / resistance;
  return amperage;
}

float Ohm::wv2a(float watts, float volts) {
  float amperage = watts / volts;
  return amperage;
}

float Ohm::wr2a(float watts, float resistance) {
  float amperage = sqrt(watts / resistance);
  return amperage;
}

float Ohm::va2r(float volts, float amps) {
  float resistance = volts / amps;
  return resistance;
}

float Ohm::vw2r(float volts, float watts) {
  float resistance = pow(volts,2) / watts;
  return resistance;
}

float Ohm::wa2r(float watts, float amps) {
  float resistance = watts / pow(amps,2);
  return resistance;
}

float Ohm::rw2v(float resistance, float wattage) {
  float voltage = sqrt(resistance * wattage);
  return voltage;
}

float Ohm::wa2v(float watts, float amps) {
  float voltage = watts / amps;
  return voltage;
}

float Ohm::ra2v(float resistance, float amps) {
  float voltage = resistance * amps;
  return voltage;
}
