#include "Button.h"

// Public Class Methods

void Button::poll() {
  int reading = digitalRead(pinId);

  /* FIXME: this branching logic is a bit hard on the eyes, it would be nice
     if we could break this out into smaller (possibly private) methods. */ 

  if (reading != lastButtonState) {
    lastDebounceTime = millis();
  }

  if (getDebounceElapsedTime() > debounceDelay) {
    if (reading != buttonState) {
      buttonState = reading;

      if (buttonState == buttonDownState) {
        lastPressedTime = millis();
      }
    }
  }

  lastButtonState = reading;

}

bool Button::isPressed() {
  if (getState() == buttonDownState) {
    return true;
  } else {
    return false;
  }
}

int Button::getState() {
  poll();
  return buttonState;
}

long Button::getDebounceElapsedTime() {
  return millis() - lastDebounceTime;
}

long Button::getStateElapsedTime() {
  return millis() - lastPressedTime;
}

void Button::setPinInputMode(int mode) {
  this->pinInputMode = mode;
}

void Button::setPinDownState(int state) {
  this->buttonDownState = state;

}
void Button::setDebounceDelay(int delay) {
  this->debounceDelay = delay;
}

void Button::setPin(int digitalPin) {
  this->pinId = digitalPin;
  pinMode(pinId, pinInputMode);
}
