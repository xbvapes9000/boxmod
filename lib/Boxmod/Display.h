// Display.cpp

#include "U8glib.h"
#include "Battery.h"

#ifndef DISPLAY_H_
#define DISPLAY_H_


class Display
{
public:
  Display() : u8g(U8G_I2C_OPT_DEV_0|U8G_I2C_OPT_NO_ACK|U8G_I2C_OPT_FAST) {
  //u8g(U8G_I2C_OPT_NONE|U8G_I2C_OPT_DEV_0) {
    u8g.setColorIndex(1);
  };
  void drawLogo();
  void drawBatt(int fill);
  void drawOhms(float resistance);
  void renderVV(float resistance, float voltage, float wattage);
  void renderVW(float resistance, float voltage, float wattage);
  Battery batt;
private:
  U8GLIB_SSD1306_128X64 u8g;
};



#endif /* DISPLAY_H_ */
