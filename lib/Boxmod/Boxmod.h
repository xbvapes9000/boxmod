#ifndef BOXMOD_H_
#define BOXMOD_H_
#include "Button.h"
#include "Display.h"
#include "Atomizer.h"
#include "Battery.h"

#define BOXMOD_BUTTON_UPDATE  0b0000001
#define BOXMOD_LCD_UPDATE     0b0000010 
#define BOXMOD_ATTY_UPDATE    0b0000100

class Boxmod
{
public:
  void tick();
  void begin();
  void setBattPin(int analogRead);
  void setUpPin(int digitalPin, int mode, int state);
  void setDownPin(int digitalPin, int mode, int state);
  void setFirePin(int digitalPin, int mode, int state);
  void setAtomizerPin(int analogPin);
  void setOutputAmpLimit(float limit);
  void setOutputVoltLimit(float limit);
  void setDefaultWattage(float limit);
 // void setBattPin(int analogPin);
  Boxmod() {
    // Set some sane defaults here, just in case
    // they don't get overriden in setup.
    wattage  = 10;
    maxAmps  = 10.0;
    maxVolts = 7.5; 
    newTime  = 0;
  };
  Button upButton;
  Button downButton;
  Button fireButton;
  Atomizer atomizer;
  Display lcd;
  Battery batt;
private:
  unsigned char bUpdates; // bitwise storage for updates
  int lcdMode; // 1 = normal/disp, 2 = fire/counter
  int atomizerPin;
  int runUpdates();
  int updateCount();
  float wattage;
  float maxAmps;
  float maxVolts;
  unsigned long newTime;
  unsigned long oldTime;
  unsigned long deltaTime;
  unsigned long calcInternalDeltaTime();
  bool fire;
  bool hasUpdates();
  void printDebugInfo();
  void handleInput();
};



#endif /* BOXMOD_H_ */
