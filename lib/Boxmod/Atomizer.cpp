#include "Atomizer.h"
#include "Ohm.h"
 
// Public Class Methods
float Atomizer::pollResistance() {
  // FIXME: this is a mock, actually implement
  // four-wire kelvin sensing on the atomizer.
  float read_value = 0.35;

  circuit.setOhms(read_value);
  return read_value;
}

bool Atomizer::isFiring() {
  return this->fireState;
}

void Atomizer::setFire(bool fire) {
  this->fireState = fire;
}

void Atomizer::setPin(int analogPin) {
  this->pinId = analogPin;
}

/* FIXME: we should only allow the atomizer to be initialized
   in a single mode at a time, so write some way to enforce
   VV/VW modes, so you can't set a disparate voltage and wattage */

void Atomizer::setFiringWattage(float wattage) {
  circuit.setWatts(wattage);
}

float Atomizer::getFiringWattage() {
  return circuit.getWatts();
}

void Atomizer::setFiringVoltage(float voltage) {
  circuit.setVolts(voltage);
}

float Atomizer::getFiringVoltage() {
  return circuit.getVolts();
}
