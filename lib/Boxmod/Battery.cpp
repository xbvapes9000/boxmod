
#include "Battery.h"
#include <Arduino.h>

//int battSensor = analogRead(14);
//// we don't need 'real' volt values if we only use them to draw a box...
//float battVolt = battSensor * (5.0 / 1023.0);

//void Battery::setBattPin(int analogPin) {
  //this->pinId = analogPin;
//}
void Battery::setPin(int analogPin) {
  this->pinId = analogPin;
}

int Battery::battFill() {
  int battRead = analogRead(pinId);
  int battIcon;
  if (battRead < 690) { 
    battIcon = 0;
  } else if (battRead > 865) { 
    battIcon = 11;
  } else {
    battIcon = map(battRead, 690, 865, 0, 11);
  }
  return battIcon;
}
