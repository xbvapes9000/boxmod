/*
 * ohm.h
 *
 *  Created on: Jul 24, 2015
 *      Author: Igneous
 */

#ifndef OHM_H_
#define OHM_H_

class Ohm
{
public:
  void  setWatts(float watts);
  void  setVolts(float volts);
  void  setAmps(float amps);
  void  setOhms(float ohms);
  float getWatts();
  float getVolts();
  float getAmps();
  float getOhms();
  Ohm() {
    setitems = 0;
    wattsSet = false;
    voltsSet = false;
    ampsSet = false;
    ohmsSet = false;
  };
private:
  int   setitems;
  float watts;
  bool  wattsSet;
  float volts;
  bool  voltsSet;
  float amps;
  bool  ampsSet;
  float ohms;
  bool  ohmsSet;
  float rw2v(float resistance, float watts);
  float wa2v(float watts, float amps);
  float ra2v(float resistance, float amps);
  float va2w(float volts, float amps);
  float ra2w(float resistance, float amps);
  float vr2w(float volts, float resistance);
  float vr2a(float volts, float resistance);
  float wv2a(float watts, float volts);
  float wr2a(float watts, float resistance);
  float va2r(float volts, float amps);
  float vw2r(float volts, float watts);
  float wa2r(float watts, float amps);
};



#endif /* OHM_H_ */
