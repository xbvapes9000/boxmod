// Display.cpp

#include "Display.h"
#include "Battery.h"
#include "icons.h"
#include <Arduino.h>

char ohm_charray[4];
char volt_charray[4];
char watt_charray[5];

 
////for debugs...  
  //u8g.drawStr(50, 62, dtostrf(batt.battFill(),6,2,batt_charray));

void Display::drawLogo() {
  u8g.firstPage();
  do {
    u8g.drawXBMP(0,0,128,64,fallen_bits);
  } while ( u8g.nextPage() );
}

void Display::drawBatt(int fill) {
	u8g.drawXBMP(1,2,16,8,batt_bits);
	u8g.drawBox(3,4,fill,4);
}

void Display::drawOhms(float resistance) {
	//char ohm_charray[4];  // why can't this go here??
	u8g.drawStr( 32, 10, dtostrf(resistance,4,2,ohm_charray));
	u8g.drawXBMP( 58, 3, 8, 8, ohm_bits);
}

/* We aren't actually doing anything with these arguments at the moment.
   I think because you hardcoded values into drawVV() just for testing.
   Now we can easily pass these on to drawVV() now that we have access
   to them (as they're being passed to us from a higher scope) */
   
void Display::renderVV(float resistance, float voltage, float wattage) {
  u8g.firstPage();
  do {
    u8g.setFont(u8g_font_fixed_v0r);
    drawBatt(batt.battFill());
    drawOhms(resistance);
    u8g.drawStr( 76, 10, dtostrf(wattage,4,2,watt_charray));
    u8g.drawStr(104, 10, "w");
    u8g.setFont(u8g_font_freedoomr10r);
    u8g.drawStr(96, 50, "V");
    u8g.setFont(u8g_font_freedoomr25n);
    u8g.drawStr( 10, 50, dtostrf(voltage,5,2,volt_charray));
  } while( u8g.nextPage() );
}

void Display::renderVW(float resistance, float voltage, float wattage) {
  u8g.firstPage();
  do {
    u8g.setFont(u8g_font_fixed_v0r);
    drawBatt(batt.battFill());
    drawOhms(resistance);
    u8g.drawStr( 76, 10, dtostrf(voltage,4,2,volt_charray));
    u8g.drawStr(104, 10, "v");
    u8g.setFont(u8g_font_freedoomr10r);
    u8g.drawStr(98, 50, "W");
    u8g.setFont(u8g_font_freedoomr25n);
    u8g.drawStr( 10, 50, dtostrf(wattage,5,2,watt_charray));
  } while( u8g.nextPage() );
}
