#include <Arduino.h> 
#include "Boxmod.h"

// Public Class Methods

void Boxmod::tick() {

  handleInput();

  if (this->hasUpdates()) {
    // Might as well boop the atomizer while we're doing work
    atomizer.pollResistance();
    runUpdates();
  } /* else {
    if (this->inputSilenceThresholdExceeded()) {
      enterLowPowerMode();
    }
  } */

}

void Boxmod::begin() {
  lcd.drawLogo();
  delay(2300);
  atomizer.pollResistance();
  atomizer.setFiringWattage(wattage);
  lcd.renderVW(atomizer.pollResistance(), atomizer.getFiringVoltage(), atomizer.getFiringWattage());
}

void Boxmod::setBattPin(int analogPin) {
  this->batt.setPin(analogPin);
}

void Boxmod::setUpPin(int digitalPin, int mode, int state) {
  this->upButton.setPin(digitalPin);
};

void Boxmod::setDownPin(int digitalPin, int mode, int state) {
  this->downButton.setPin(digitalPin);
};

void Boxmod::setFirePin(int digitalPin, int mode, int state) {
  this->fireButton.setPin(digitalPin);
};

void Boxmod::setAtomizerPin(int analogPin) {
  this->atomizer.setPin(analogPin);
};

void Boxmod::setOutputAmpLimit(float limit) {
  this->maxAmps = limit;
};

void Boxmod::setOutputVoltLimit(float limit) {
  this->maxVolts = limit;
};

void Boxmod::setDefaultWattage(float wattage) {
  this->wattage = wattage;
};

// Private Class Methods
int Boxmod::runUpdates() {
  char updatesRun = 0;

  if (bUpdates & BOXMOD_BUTTON_UPDATE) {
    atomizer.setFiringWattage(this->wattage);

    /* Signal that we've completed the button update and need to redraw the lcd
       this is mocked out right now, once we have a global "context", ultimately
       the context will decide whether or not we need to redraw.. At the moment,
       we're just assuming that we do */
    bUpdates ^= BOXMOD_BUTTON_UPDATE;
    bUpdates |= BOXMOD_LCD_UPDATE;
    updatesRun += 1;
  };

  if (bUpdates & BOXMOD_LCD_UPDATE) {
    lcd.renderVW(atomizer.pollResistance(), atomizer.getFiringVoltage(), atomizer.getFiringWattage());

    bUpdates ^= BOXMOD_LCD_UPDATE;
    updatesRun += 1;
  };

  return updatesRun;
}

bool Boxmod::hasUpdates() {
  if (bUpdates > 0) {
    return true;
  } else {
    return false;
  };
}

void Boxmod::handleInput() {
  /* TODO: this is a super simple implementation at the moment.
     We really need to look at button chording, as well as handling
     input in a context-dependent way (e.g. the up button scrolls the
     menu when in a menu mode, incr wattage in VW mode, and incr volts in VV */

  if (upButton.isPressed()) {
    int timePressed = upButton.getStateElapsedTime();

    if (timePressed == 0)  wattage += 0.1;
    if (timePressed >= 1000 && timePressed < 4500) wattage += 0.2;
    if (timePressed >= 4500) wattage += 1.1;

    bUpdates |= BOXMOD_BUTTON_UPDATE;
  };

  if (downButton.isPressed()) {
    int timePressed = downButton.getStateElapsedTime();

    if (timePressed == 0)  wattage -= 0.1;
    if (timePressed >= 1000 && timePressed < 4500) wattage -= 0.2;
    if (timePressed >= 4500) wattage -= 1.1;

    bUpdates |= BOXMOD_BUTTON_UPDATE;
  };

  if (fireButton.isPressed()) {
    lcdMode   = 2;
    fire      = true;
    bUpdates |= BOXMOD_BUTTON_UPDATE;
  } else {
    lcdMode = 1;
    fire    = false;
  };
};
