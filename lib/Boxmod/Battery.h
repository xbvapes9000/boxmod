

#ifndef BATTERY_H_
#define BATTERY_H_

class Battery
{
public:
	void setPin(int analogPin);
	int battFill();
private:
	int pinId;
};


#endif /* BATTERY_H_ */
