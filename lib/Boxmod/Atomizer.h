#ifndef ATOMIZER_H_
#define ATOMIZER_H_

#include "Ohm.h"

class Atomizer
{
public:
  float pollResistance();
  float getFiringWattage();
  float getFiringVoltage();
  void setPin(int analogPin);
  void setFire(bool fire);
  void setFiringVoltage(float voltage);
  void setFiringWattage(float wattage);
  bool isFiring();
private:
  int pinId;
  bool fireState;
  float resistance;
  Ohm circuit;
};



#endif /* ATOMIZER_H_ */
