#include "Boxmod.h"

Boxmod boxmod;

void setup() {
  // Boxmod setup
  boxmod.setUpPin(2, INPUT, HIGH);
  boxmod.setDownPin(3, INPUT, HIGH);
  boxmod.setFirePin(4, INPUT, HIGH);
  boxmod.setAtomizerPin(1);
  boxmod.setBattPin(14);
  boxmod.setOutputAmpLimit(20);
  boxmod.setOutputVoltLimit(7.5);
  boxmod.setDefaultWattage(10.0);

  boxmod.begin();
}

void loop() {
  boxmod.tick();
}
